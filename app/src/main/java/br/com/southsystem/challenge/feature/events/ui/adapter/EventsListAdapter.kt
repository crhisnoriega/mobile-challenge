package br.com.southsystem.challenge.feature.events.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.southsystem.challenge.databinding.ItemEventLayoutBinding
import br.com.southsystem.challenge.feature.events.model.EventDTO
import br.com.southsystem.challenge.feature.events.ui.fragments.EVENT_ACTION
import br.com.southsystem.challenge.feature.events.ui.viewholder.ItemEventViewHolder

class EventsListAdapter(
    val eventsList: MutableList<EventDTO>,
    private val onClick: ((currencyEntity: EventDTO, eventAction: EVENT_ACTION) -> Unit)?
) :
    RecyclerView.Adapter<ItemEventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemEventViewHolder(
            ItemEventLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun getItemCount() = eventsList.size

    override fun onBindViewHolder(holder: ItemEventViewHolder, position: Int) {
        holder.bind(eventsList[position], onClick)
    }
}