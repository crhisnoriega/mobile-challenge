package br.com.southsystem.challenge.feature.events.business

import br.com.southsystem.challenge.base.BaseUseCase
import br.com.southsystem.challenge.data.Resource
import br.com.southsystem.challenge.feature.events.model.EventDTO
import br.com.southsystem.challenge.feature.events.repository.EventsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class RetrieveEventUseCase(
    private val eventsRepository: EventsRepository
) : BaseUseCase<String, EventDTO>(
    Dispatchers.IO
) {
    override fun execute(eventID: String): Flow<Resource<EventDTO>> {
        return eventsRepository.retrieveEvent(eventID).map {
            Resource.success(it)
        }
    }
}