package br.com.southsystem.challenge.data

import android.accounts.NetworkErrorException
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException


inline fun <T> parserResponse(json: String, classs: Class<T>): T {
    return Gson().fromJson(
        json, classs
    )
}

suspend inline fun <T> safeAPICall(
    request: suspend () -> Flow<T>,
    crossinline onResponse: (Resource<T>) -> Unit
) {

    request.invoke()
        .flowOn(Dispatchers.IO)
        .catch { throwable ->
            when (throwable) {
                is HttpException -> {
                    when (throwable.code()) {
                        400, 401 -> onResponse.invoke(Resource.error(throwable.message()))
                    }
                }

                is NetworkErrorException, is java.net.UnknownHostException ->
                    onResponse.invoke(Resource.error(throwable.message ?: ""))

                else -> onResponse.invoke(Resource.error(""))
            }

        }.collect {
            onResponse.invoke(Resource.success(it))
        }


}