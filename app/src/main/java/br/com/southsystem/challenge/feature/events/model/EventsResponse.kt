package br.com.southsystem.challenge.feature.events.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class EventsResponse(
    @SerializedName("data") val data: List<EventDTO>?
) : Parcelable