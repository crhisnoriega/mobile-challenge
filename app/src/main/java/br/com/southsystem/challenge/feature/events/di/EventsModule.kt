package br.com.southsystem.challenge.feature.events.di

import androidx.room.Room
import br.com.southsystem.challenge.data.AppDatabase
import br.com.southsystem.challenge.data.DATABASE_NAME
import br.com.southsystem.challenge.feature.events.api.EventsAPI
import br.com.southsystem.challenge.feature.events.business.DoCheckInUseCase
import br.com.southsystem.challenge.feature.events.business.FetchEventsUseCase
import br.com.southsystem.challenge.feature.events.business.RetrieveEventUseCase
import br.com.southsystem.challenge.feature.events.repository.EventsRepository
import br.com.southsystem.challenge.feature.events.repository.EventsRepositoryImpl
import br.com.southsystem.challenge.feature.events.viewmodel.CheckInViewModel
import br.com.southsystem.challenge.feature.events.viewmodel.EventsListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object EventsModule {
    val modules = module {
        // api
        factory { providerEventsAPI(get()) }

        // repository
        factory<EventsRepository> { EventsRepositoryImpl(get()) }

        // business
        factory { FetchEventsUseCase(get(), get()) }
        factory { RetrieveEventUseCase(get()) }
        factory { DoCheckInUseCase(get()) }

        // database - DAO
        single {
            Room.databaseBuilder(
                androidContext(),
                AppDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
        single { get<AppDatabase>().eventDAO() }

        // view models
        viewModel { EventsListViewModel(get(), get()) }
        viewModel { CheckInViewModel(get()) }
    }

    private fun providerEventsAPI(retrofit: Retrofit) = retrofit.create(EventsAPI::class.java)
}