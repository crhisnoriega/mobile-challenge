package br.com.southsystem.challenge.feature.events.repository

import br.com.southsystem.challenge.feature.events.api.EventsAPI
import br.com.southsystem.challenge.feature.events.model.EventDTO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response


interface EventsRepository {
    fun fetchEvents(): Flow<List<EventDTO>>

    fun retrieveEvent(eventID: String): Flow<EventDTO>

    fun doCheckIn(eventDTO: EventDTO): Flow<Response<Any>>
}

class EventsRepositoryImpl(private val eventsAPI: EventsAPI) :
    EventsRepository {

    override fun fetchEvents() = flow {
        emit(eventsAPI.fetchEvents())
    }

    override fun retrieveEvent(eventID: String) = flow {
        emit(eventsAPI.retrieveEvent(eventID))
    }

    override fun doCheckIn(eventDTO: EventDTO) = flow {
        emit(eventsAPI.doCheckIn(eventDTO))
    }

}