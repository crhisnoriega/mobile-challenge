package br.com.southsystem.challenge.feature.events.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import br.com.southsystem.challenge.base.BaseFragment
import br.com.southsystem.challenge.data.Status
import br.com.southsystem.challenge.databinding.FragmentEventDetailsBinding
import br.com.southsystem.challenge.feature.events.model.EventDTO
import br.com.southsystem.challenge.feature.events.viewmodel.EventsListViewModel
import br.com.southsystem.challenge.util.argument
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.android.viewmodel.ext.android.sharedViewModel

class EventDetailsFragment : BaseFragment() {

    companion object {
        const val EVENT_ID_ARG = "EVENT_ID_ARG"

        fun newInstance(eventId: String) =
            EventDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(EVENT_ID_ARG, eventId)
                }
            }
    }

    private val viewModel by sharedViewModel<EventsListViewModel>()
    private val eventID: String by argument(
        EVENT_ID_ARG
    )

    private lateinit var binding: FragmentEventDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureObservers()

        viewModel.fetchEvent(eventID)
    }

    private fun configureObservers() {
        viewModel.eventResult.observe(requireActivity(), Observer { data ->
            when (data.status) {
                Status.LOADING -> binding.progress.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    populateData(data.data)
                    binding.progress.visibility = View.GONE
                }
                Status.ERROR -> {
                    binding.progress.visibility = View.GONE
                    showError(data.message, binding.snackBar)
                }
            }
        })
    }

    private fun populateData(eventDTO: EventDTO?) {
        binding.title.text = eventDTO?.title
        binding.description.text = eventDTO?.description

        context?.let {
            Glide.with(it).load(eventDTO?.image).into(binding.image)
        }
    }

}