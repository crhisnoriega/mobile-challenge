package br.com.southsystem.challenge.feature.events.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import br.com.southsystem.challenge.databinding.ItemEventLayoutBinding
import br.com.southsystem.challenge.feature.events.model.EventDTO
import br.com.southsystem.challenge.feature.events.ui.fragments.EVENT_ACTION

class ItemEventViewHolder(private val itemEventLayoutBinding: ItemEventLayoutBinding) :
    RecyclerView.ViewHolder(itemEventLayoutBinding.root) {

    fun bind(
        eventDTO: EventDTO,
        onClick: ((eventDTO: EventDTO, eventAction: EVENT_ACTION) -> Unit)?
    ) {
        itemEventLayoutBinding.title.text = eventDTO.title
        itemEventLayoutBinding.description.text = eventDTO.description
        itemEventLayoutBinding.btnCheckin.setOnClickListener {
            onClick?.invoke(eventDTO, EVENT_ACTION.CHECK_IN)
        }

        itemEventLayoutBinding.btnDetails.setOnClickListener {
            onClick?.invoke(eventDTO, EVENT_ACTION.DETAILS)
        }
    }

}